package com.firstgametest;

/**
 * A file for constants within the game.
 *
 * Created by jvullo on 31/05/15.
 */
public class Constants {

    public static final boolean DEBUG_ON = false;
    public static final String APP_NAME = "TEST GAME";

    // Preference Keys
    public static final String HIGHSCORE_KEY = "HIGHSCORE";




}
