package com.firstgametest.game;

import com.badlogic.gdx.Game;
import com.firstgametest.gamehelpers.AssetLoader;
import com.firstgametest.screen.SplashScreen;

public class GameMain extends Game {

	@Override
	public void create() {
		//Gdx.app.log("GAME_MAIN", "Created");
		AssetLoader.load();
		//setScreen(new GameScreen());
		setScreen(new SplashScreen(this));
	}

	@Override
	public void dispose() {
		super.dispose();
		AssetLoader.dispose();
	}
}
