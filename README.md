# README #

### What is this repository for? ###

This repo stores the code I built following the flappy bird clone tutorial from
http://www.kilobolt.com/. Screenshots of the final application can be found in the
download sections.

### How do I get set up? ###

The project was created using intellij and libGDX.

#### SOFTWARE/IDE ####

 * Intellij
 * Download the Setup App - http://libgdx.badlogicgames.com/download.html
 * Run the java jar, this should be pretty straightforward.
 * Configure intellij with the libGDX library ???
 * Also needs the tween library added to the project
   - Download this: https://code.google.com/p/java-universal-tween-engine/downloads/detail?name=tween-engine-api-6.3.3.zip&can=2&q=
   - And follow the gradle instructions from here: https://github.com/libgdx/libgdx/wiki/Universal-Tween-Engine

   This should be added in the gradle file for the project, like so:
   - compile fileTree(dir: "libs", include: "*.jar")

   The following statement should be added within "dependencies" of the :desktop, :android & :core.

   I then added the project info manually because that seemed to do nothing.
   File -> Project Structure -> Modules and for Android, Core, Desktop click the little plus
   and then add the "libs/relevant.jars" and you are good to go.

#### Asset Problems ####

You need to create a symbolic link so that you use the android/assets
folder for android applications and desktop applications.
Something like:

   ln -s android/assets/data data.

